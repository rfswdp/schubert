# Schubert Toolbox

This project intends to be my personal toolbox for solutions of day to day issues.

* `from schubert.itau import calculate_dac` -> Calculate DAC for Itau Invoices
* `from schubert.febraban import generate_codebar_from_bank_slip_digitable_line` -> Get numeric representation of Codebar from Bank Slip digitable line
* `from schubert.febraban import generate_codebar_from_utility_slip_digitable_line` -> Get numeric representation of Codebar from Utility Slip digitable line


### Installation

To install this `pip` package you need just install as any package.

```shell
> pip install schubert
```
